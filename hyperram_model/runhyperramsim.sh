#!/bin/bash
# https://ftp.libre-soc.org/Infineon-Verilog_Model_for_HyperBus_interface-SimulationModels-v03_00-EN.zip
set -e

LIB_DIR=./s27kl0641/model

# string together the icarus verilog files and start runnin
iverilog -I${LIB_DIR} -Wall -g2012 -s hbc_tb -o hypersim \
    hbc_tb.v \
    hbc_wrapper.v \
    hbc_io.v \
    hbc.v \
    ${LIB_DIR}/s27kl0641.v
vvp -n hypersim -fst-speed
