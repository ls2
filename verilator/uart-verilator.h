// Copyright (C) IBM 2019, see LICENSE.CC4
// based on code from microwatt https://github.com/antonblanchard/microwatt

/*
 * Our UART uses 16x oversampling, so at 50 MHz and 115200 baud
 * each sample is: 50000000/(115200*16) = 27 clock cycles. This
 * means each bit is off by 0.47% so for 8 bits plus a start and
 * stop bit the errors add to be 4.7%.
 */

enum state {
    IDLE, START_BIT, BITS, STOP_BIT, ERROR
};

struct uart_tx_state {
    double error = 0.05;

    enum state tx_state = IDLE;
    unsigned long tx_countbits;
    unsigned char tx_bits;
    unsigned char tx_byte;
    unsigned char tx_prev;

    enum state rx_state = IDLE;
    unsigned char rx_char;
    unsigned long rx_countbits;
    unsigned char rx_bit;
    unsigned char rx = 1;
};

void uart_tx(unsigned char tx);
unsigned char uart_rx(void);
struct uart_tx_state * uart_get_state(void);
void uart_restore(struct uart_tx_state *);

