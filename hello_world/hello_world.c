#include <stdint.h>
#include <stdbool.h>

#include "console.h"

static char mw_logo[] =

"\n"
"   .oOOo.     \n"
" .\"      \". \n"
" ;  .mw.  ;   Microwatt, it works.\n"
"  . '  ' .    \n"
"   \\ || /    \n"
"    ;..;      \n"
"    ;..;      \n"
"    `ww'   \n";

static inline uint32_t readl(unsigned long addr)
{
    uint32_t val;
    __asm__ volatile("sync; lwzcix %0,0,%1" : "=r" (val) : "r" (addr) : "memory");
    return val;
}

static inline void writel(uint32_t val, unsigned long addr)
{
    __asm__ volatile("sync; stwcix %0,0,%1" : : "r" (val), "r" (addr) : "memory");
}

void uart_writeuint32(uint32_t val) {
    const char lut[] = { '0', '1', '2', '3', '4', '5', '6', '7',
                         '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
    uint8_t *val_arr = (uint8_t*)(&val);
    size_t i;

    for (i = 0; i < 4; i++) {
        putchar(lut[(val_arr[3-i] >> 4) & 0xF]);
        putchar(lut[val_arr[3-i] & 0xF]);
    }
}


int main(void)
{
	console_init();

	puts(mw_logo);

    volatile uint32_t *sram = 0x0;
    int count = 26;
	puts("writing\n");
    for (int i = 0; i < count; i++) {
        uart_writeuint32(i);
        puts("\n");
        writel(0xBEEF0000+i, &(sram[1<<i]));
    }
	puts("reading\n");
    for (int i = 0; i < count; i++) {
        int val = readl(&(sram[1<<i]));
        uart_writeuint32(i);
        puts(" ");
        uart_writeuint32(val);
        puts("\n");
    }
	while (1) {
		unsigned char c = getchar();
		putchar(c);
		if (c == 13) // if CR send LF
			putchar(10);
	}
}
