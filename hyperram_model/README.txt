1) download the Cypress HyperRAM Model
   http://www.cypress.com/verilog/s27kl0641-verilog
2) download the Winbond HyperRAM Model
   https://www.winbond.com/resource-files/W956x8MBY_verilog_p.zip
3) install icarus verilog
4) run ./runhyperramsim.sh

Cypress HyperRAM Model files are
Copyright (C) 2015 Spansion, LLC.
(no explicit license found, but they are available publicly for download)

Winbond HyperRAM Model files are
Copyright C 2019 Winbond Electronics Corp. All rights reserved.
(no explicit license found, but they are available publicly for download)

hbc_*.v files from https://github.com/gtjennings1/HyperBUS are
Copyright 2017 Gnarly Grey LLC and have been released under this
license by Gnarly Grey:

// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:

// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
// IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
// CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
