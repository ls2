python3 src/ls2.py versa_ecp5 ./hello_world/hello_world.bin
openocd -f top-openocd.cfg \
        -c "transport select jtag; init; svf -quiet top.svf; exit"

# Versa ECP5 ecpprog QSPI

ecpprog -o 0 binary.bin

# Arty A7-100t

* apt install gcc-powerpc64le-linux-gnu
* git clone -b microwatt-5.7 https://git.kernel.org/pub/scm/linux/kernel/git/joel/microwatt.git
* cd microwatt
* CROSS_COMPILE="ccache powerpc64le-linux-gnu-" ARCH=powerpc make -j8 O=microwatt
* git clone https://github.com/antonblanchard/microwatt
* git clone https://github.com/quartiq/bscan_spi_bitstreams
  and update openocd/bscan_spi_xc7a100t.bin in microwatt
* python3 openocd/flash-arty -f a100 -a 0x600000 -t bin dtbImage.microwatt
* xc3sprog -c nexys4 libresoc.bit (more likely, build/top.bit)
