# this is a quick demo of the Clock-Reset-Generator which can
# control the Blinky clock.

from nmigen import Elaboratable, Module
from nmigen_boards.test.blinky import Blinky
from nmigen_boards.arty_a7 import ArtyA7_100Platform
from arty_crg import ArtyA7CRG

class BlinkyClocked(Elaboratable):
    def elaborate(self, platform):
        m = Module()
        m.submodules.crg = ArtyA7CRG(12e6)
        m.submodules.blinky = Blinky()
        return m

if __name__ == "__main__":
    ArtyA7_100Platform(toolchain="yosys_nextpnr").build(BlinkyClocked(),
                        do_program=True)
